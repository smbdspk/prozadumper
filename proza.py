#!/usr/bin/env python3

import asyncio
# import requests
import os
import re
# import random
import time
from socket import error as SocketError

import aiohttp
from aiohttp_retry import RetryClient, RetryOptions
from bs4 import BeautifulSoup
from tqdm import tqdm

from aiohttp_socks import ProxyType, ProxyConnector, ChainProxyConnector

# import errno

start_year = 2001
end_year = 2020
end_month = 12
end_day = 31

genre_dic = {"миниатюры": "Малые формы",
             "новеллы": "Малые формы",
             "рассказы": "Малые формы",
             "репортажи": "Малые формы",
             "повести": "Крупные формы",
             "романы": "Крупные формы",
             "драматургия": "Жанровые произведения",
             "детективы": "Жанровые произведения",
             "приключения": "Жанровые произведения",
             "фантастика": "Жанровые произведения",
             "фэнтези": "Жанровые произведения",
             "ужасы": "Жанровые произведения",
             "киберпанк": "Жанровые произведения",
             "эротическая проза": "Жанровые произведения",
             "юмористическая проза": "Юмор",
             "ироническая проза": "Юмор",
             "фельетоны": "Юмор",
             "анекдоты": "Юмор",
             "байки": "Юмор",
             "история и политика": "Эссе и статьи",
             "литературоведение": "Эссе и статьи",
             "естествознание": "Эссе и статьи",
             "публицистика": "Эссе и статьи",
             "философия": "Эссе и статьи",
             "религия": "Эссе и статьи",
             "мистика": "Эссе и статьи",
             "мемуары": "Эссе и статьи",
             "критические статьи": "Литературная критика",
             "литературные обзоры": "Литературная критика",
             "музыкальные и кинообзоры": "Литературная критика",
             "литература для детей": "Детские разделы",
             "рассказы о детях": "Детские разделы",
             "сказки": "Детские разделы",
             "детское творчество": "Детские разделы",
             "стихи": "Поэзия",
             "стихотворения в прозе": "Поэзия",
             "литературные переводы": "Переводы и проза на других языках",
             "проза на других языках": "Переводы и проза на других языках"}

rubrics = {"05": "миниатюры",
           "21": "новеллы",
           "02": "рассказы",
           "30": "репортажи",
           "01": "повести",
           "04": "романы",
           "13": "драматургия",
           "07": "детективы",
           "23": "приключения",
           "06": "фантастика",
           "24": "фэнтези",
           "25": "ужасы",
           "26": "киберпанк",
           "03": "эротическая проза",
           "08": "юмористическая проза",
           "16": "ироническая проза",
           "09": "фельетоны",
           "27": "анекдоты",
           "28": "байки",
           "31": "история и политика",
           "10": "литературоведение",
           "32": "естествознание",
           "11": "публицистика",
           "33": "философия",
           "34": "религия",
           "35": "мистика",
           "18": "мемуары",
           "12": "критические статьи",
           "41": "литературные обзоры",
           "42": "музыкальные и кинообзоры",
           "17": "литература для детей",
           "51": "рассказы о детях",
           "52": "сказки",
           "50": "детское творчество",
           "39": "стихи",
           "43": "стихотворения в прозе",
           "15": "литературные переводы",
           "44": "проза на других языках"}

# задаем хэдеры
user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/600.3.18 (KHTML, like Gecko) Version/8.0.3 Safari/600.3.18'
headers = {'User-Agent': user_agent}

# логи
import logging

FORMAT = '%(asctime)s [%(levelname)s] (%(name)s) %(filename)s:%(lineno)s: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger("asyncio").setLevel(logging.ERROR)
log = logging.getLogger('mainApp')

# асинх
meta_lock = asyncio.Lock()
failed_lock = asyncio.Lock()
topic_sem = asyncio.Semaphore(10)
link_sem = asyncio.Semaphore(10)
dlink_sem = asyncio.Semaphore(5)

# прокся
conn = None
# conn = ProxyConnector.from_url('socks5://127.0.0.1:8888')


async def createSession():
#    return aiohttp.ClientSession(headers=headers)
    retry_options = RetryOptions(attempts=10, statuses={503, 563}, start_timeout=0.8, factor=1,
                                 exceptions={aiohttp.client_exceptions.ServerDisconnectedError,
                                             aiohttp.client_exceptions.ClientConnectorError,
                                             ConnectionResetError,
                                             SocketError})
    return RetryClient(retry_options=retry_options, headers=headers, connector=conn)


def ensureDir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory, mode=0o777, exist_ok=False)
    return directory


def makeDailyLink(year, month, day, topic='all'):
    return 'http://www.proza.ru/texts/list.html?day=' + str(day) + '&month=' + str(month) + '&year=' + str(
        year) + '&topic=' + str(topic)


# def getNPoems(link):
#    r = requests.get(link, headers=headers)
#    num = int(re.split(' по', re.split('<p>Произведения в обратном порядке с ', r.text)[1])[0])
#    return num

async def getPoemLinks(link):
    # ссылка на текст, заголовок, имя автора, ссылка на автора, дата и время
    text = ""
    # print("getLinks is:", link_sem._value)
    await link_sem.acquire()
    # print("Locking getLinks:", link_sem._value)
    async with retry_client.get(link) as r:
        text = await r.text()
    link_sem.release()
    # print("Unlocking getLinks:", link_sem._value)
    links = re.split('"textlink nounline', re.split('опубликовать произведение', text)[1])[0].split('</ul>')
    textinfo = []

    for part in links:
        if 'Авторские анонсы' not in part:
            for l in part.split('\n'):
                if "poemlink" in l:
                    poemlink = 'http://www.proza.ru' + re.split('" ', re.split('<li><a href="', l)[1])[0]
                    title = re.split('</a>', re.split('class="poemlink">', l)[1])[0]
                    author = re.split('</a>', re.split('class="authorlink">', l)[1])[0]
                    authorlink = 'http://www.proza.ru/avtor/' + \
                                 re.split('" class="authorlink', re.split('href="/avtor/', l)[1])[0]
                    datetime = re.split('</small></li>', re.split('<small>- ', l)[1])[0]
                    date = datetime.split()[0]
                    time = datetime.split()[1]
                    textinfo.append([poemlink, title, author, authorlink, date, time])
    return textinfo


async def getPoemLinksByDate(daily_link):
    # print(f"Получаем ссылки дня: {time.time() - start:.10f}")
    text = ""
    # print("getDateLinks is:", dlink_sem._value)
    await dlink_sem.acquire()
    # print("Locking getDateLinks:", link_sem._value)
    async with retry_client.get(daily_link) as r:
        text = await r.text()
    dlink_sem.release()
    # print("Unlocking getDateLinks:", dlink_sem._value)
    text_info = []

    lines = text.split('\n')

    starts = []
    for l in lines:
        if daily_link.strip('http://www.proza.ru' + '&start=') in l:
            starts += [
                'http://www.proza.ru' + re.split('">', re.split('<a style="text-decoration:none" href="', l)[1])[0]]

    # print(f"Получаем инфу текста: {time.time() - start:.10f}")
    text_info = await getPoemLinks(daily_link)
    for t in await asyncio.gather(*[getPoemLinks(i) for i in starts]):
        text_info += t

    # tasks = []
    return text_info


def makePoemLink(year, month, day, textid):
    if year < 2008 and int(month) < 10:
        try:
            textlink = 'http://www.proza.ru/' + str(year) + '/' + str(month) + '/' + str(day) + '-' + str(textid)
        except:
            textlink = 'http://www.proza.ru/' + str(year) + '/' + str(month) + '/' + str(day) + '/' + str(textid)
    else:
        textlink = 'http://www.proza.ru/' + str(year) + '/' + str(month) + '/' + str(day) + '/' + str(textid)
    return textlink


# Функия принимает адрес статьи и возвращает текст статьи и метаинформацию по ней.
async def getTextProza(textlink):
    text = ""
    # print("getText is:", topic_sem._value)
    await topic_sem.acquire()
    # print("Locking getText:", topic_sem._value)
    try:
        async with retry_client.get(textlink) as r:
            text = await r.text()
    except Exception as e:
        raise e
    finally:
        topic_sem.release()
    # print("Unlocking getText:", topic_sem._value)
    text = re.split("</div>", re.split('<div class="text">', text)[1])[0]
    # "Откусываем" оставшиеся теги.
    beaux_text = BeautifulSoup(text, "lxml")
    n_text = beaux_text.get_text()
    n_text = re.sub('\xa0', '', n_text)
    # n_text = unify.unify_sym(n_text)
    return (n_text)


# Теперь список авторов нам нужно превратить с список данных автора и ссылки на его тексты:
# def getAuthorInfo(authorlink):
#     r = requests.get(authorlink, headers=headers)
#     try:
#         author_items = re.split("</b>", re.split("Произведений: <b>", r.text)[1])[0]
#         author_readeres = re.split("</b>", re.split("Читателей</a>: <b>", r.text)[1])[0]
#         return author_items, author_readeres
#     except:
#         return '', ''


WDIR = ensureDir(r'./prdump/texts')


async def main():
    for year in range(start_year, end_year + 1)[::-1]:
        await runYear(year)
    await retry_client.close()


async def runYear(year):
    metatable_texts = open(ensureDir(r'./prdump/meta/' + str(year)) + '/metatable_texts.txt', 'a', encoding='utf8')
    time.sleep(1)
    metatable_texts.write(
        'textid\tURL\ttitle\tauthor\tauthorlink\tdate\ttime\tpath\tauthor_readers\tauthor_texts\ttopic\tgenre\n')
    # textid, poemlink, title, author, authorlink,date,time, path, author_readers, author_poems,topic,genre
    for month in range(1, 13)[::-1]:
        await runMonth(year, month, metatable_texts)
    metatable_texts.close()


async def runMonth(year, month, metatable_texts):
    if month > end_month and year == end_year:
        pass
    else:
        print(f"{month}/{year}: {time.time() - start:.10f}")
        month = str(month).zfill(2)
        path = ensureDir(WDIR + "/" + str(year) + "/" + month)
        for day in tqdm(range(1, 32)[::-1]):
            await runDay(year, month, day, metatable_texts, path)


async def runDay(year, month, day, metatable_texts, path):
    if day > end_day and month == end_month and year == end_year:
        pass
    else:
        day = str(day).zfill(2)
        tasks = []
        for topic in rubrics:
            # print(year, month, day, rubrics[topic])
            tasks.append(runTopic(year, month, day, topic, metatable_texts, path))
        await asyncio.gather(*tasks)
        # print(f"День записан: ({day}/{month}/{year}): {time.time() - start:.10f}")
        # print([task for task in asyncio.all_tasks() if not task.done()])


async def runTopic(year, month, day, topic, metatable_texts, path):
    # print(f"Начинаем топик: {time.time() - start:.10f}")
    link = makeDailyLink(year, month, day, topic)
    text_info = await getPoemLinksByDate(link)  # замедляет выполнение крайне
    # print(f"Получена информация: {time.time() - start:.10f}")

    tasks = []
    for i in range(len(text_info)):
        textid = str(year) + str(month) + str(day) + str(i) + str(topic)
        text = text_info[i]
        tasks.append(runText(text, textid, topic, metatable_texts, path))
    await asyncio.gather(*tasks)
    # if len(tasks) == 0:
    #    print(f"Топик пуст/не удалось получить ({genre_dic[rubrics[topic]]}): {time.time() - start:.10f}")
    # else:
    #    print(f"Рубрика записана: {time.time() - start:.10f}")


async def runText(text_i, textid, topic, metatable_texts, path):
    while True:
        try:
            textlink = text_i[0]
            # print(f"Получаем текст: {time.time() - start:.10f}")
            text = await getTextProza(textlink)
            textfile = open(os.path.join(path, textid + '.txt'), 'w', encoding='utf8')
            textfile.write(text)
            textfile.close()
            # author_poems, author_readers = getAuthorInfo(text_i[3])
            genre = genre_dic[rubrics[topic]]
            # textfeats = [textid]+text_i + [os.path.join(path, textid+'.txt'), author_poems, author_readers, topic, genre]
            textfeats = [textid] + text_i + [os.path.join(path, textid + '.txt'), topic, genre]

            await meta_lock.acquire()
            metatable_texts.write("\t".join(textfeats) + '\n')
            meta_lock.release()
            break
        except IndexError:
            await failed_lock.acquire()
            logf.write(textlink + "\n")
            failed_lock.release()
            break
        except UnicodeDecodeError as e:
            print("Ошибка декода:", textlink, e)
            await failed_lock.acquire()
            logf.write(textlink + " (" + str(e) + ")\n")
            failed_lock.release()
            break
        except Exception as e:
            print("Что-то пошло не так:", textlink, e)
            await failed_lock.acquire()
            logf.write(textlink + " (" + str(e) + ")\n")
            failed_lock.release()


if __name__ == "__main__":
    logf = open("./prdump/err.log", "a")
    # session = asyncio.get_event_loop().run_until_complete(createSession())
    retry_client = asyncio.get_event_loop().run_until_complete(createSession())
    start = time.time()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
